FROM ubuntu:18.04

RUN apt-get update && apt-get install -y openjdk-11-jre

RUN apt-get update && apt-get install -y gnupg2
RUN apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys B97B0AFCAA1A47F044F244A07FCC7D46ACCC4CF8
RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ precise-pgdg main" > /etc/apt/sources.list.d/pgdg.list
RUN apt-get update && apt-get install -y postgresql postgresql-contrib
USER postgres
RUN    /etc/init.d/postgresql start &&\
    psql --command "CREATE USER request_stubbing WITH SUPERUSER PASSWORD 'fwHKhf9P';" &&\
    createdb -O request_stubbing request_stubbing

COPY build/libs/request-stubbing-1.0-SNAPSHOT.jar app.jar

USER root
EXPOSE 5050/tcp

RUN apt-get install -y wget
RUN wget -qO- https://repo1.maven.org/maven2/org/flywaydb/flyway-commandline/6.0.0/flyway-commandline-6.0.0-linux-x64.tar.gz | tar xvz && ln -s `pwd`/flyway-6.0.0/flyway /usr/local/bin

COPY src/main/resources/db/migration/ res/db/migration

CMD /etc/init.d/postgresql start \
&& flyway migrate -url=jdbc:postgresql://localhost:5432/request_stubbing -user=request_stubbing -password=fwHKhf9P -locations=filesystem:./res \
&& java -jar app.jar --spring.profiles.active=persistent

/**
 * Copyright (c) 2019 Lineate LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package com.lineate.request.stubbing.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.lineate.request.stubbing.entity.request.Request;
import com.lineate.request.stubbing.entity.request.RequestResponse;
import com.lineate.request.stubbing.entity.request.RequestState;
import com.lineate.request.stubbing.exception.RequestProcessingException;
import com.lineate.request.stubbing.form.RequestCreateForm;
import com.lineate.request.stubbing.form.RequestResponseCreateForm;
import com.lineate.request.stubbing.provider.RequestConfigurationProvider;
import com.lineate.request.stubbing.repository.RequestRepository;
import com.lineate.request.stubbing.repository.RequestStateRepository;
import org.apache.commons.io.IOUtils;
import org.everit.json.schema.ValidationException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import static java.io.File.createTempFile;
import static java.nio.charset.Charset.defaultCharset;
import static org.apache.commons.io.FileUtils.writeStringToFile;
import static org.everit.json.schema.loader.SchemaLoader.load;

/**
 * Service to POST request's mocks processing.
 */
@Service
public class RequestService {

    private final ObjectMapper objectMapper;
    private final RequestRepository requestRepository;
    private final RequestStateRepository requestStateRepository;

    private final RequestConfigurationProvider configurationProvider;


    public RequestService(ObjectMapper objectMapper,
                          RequestRepository requestRepository,
                          RequestStateRepository requestStateRepository, RequestConfigurationProvider configurationProvider) {

        this.objectMapper = objectMapper;
        this.requestRepository = requestRepository;
        this.requestStateRepository = requestStateRepository;
        this.configurationProvider = configurationProvider;
    }

    /**
     * Finds calls of request.
     */
    public List<RequestState> findHistory(String urlMapping) {
        Request request = requestRepository
                .findByUrlMapping(urlMapping)
                .orElseThrow(EntityNotFoundException::new);

        return requestStateRepository.findByRequest(request);
    }

    /**
     * Deletes calls of request.
     */
    public void clearHistory(String urlMapping) {
        Request request = requestRepository
                .findByUrlMapping(urlMapping)
                .orElseThrow(EntityNotFoundException::new);

        requestStateRepository.deleteByRequest(request);
    }

    /**
     * Mocks POST request with default response.
     */
    public Request request(String urlMapping,
                           Map<String, Object> params,
                           Map<String, Object> body) {

        Request request = requestRepository
                .findByUrlMapping(urlMapping)
                .orElseGet(() -> createOne(urlMapping, objectMapper.convertValue(body, RequestCreateForm.class)));

        snapshotRequest(request, params, body);
        return request;
    }

    /**
     * Unsets POST request's mock.
     */
    public void unset(String urlMapping) {
        requestRepository.deleteByUrlMapping(urlMapping);
    }

    private Map<String, Object> checkSchema(Request request, Map<String, Object> body) {
        if (null == request.getSchema()) {
            return Map.of();
        }

        try {
            load(new JSONObject(new JSONTokener(objectMapper.writeValueAsString(request.getSchema())))).validate(new JSONObject(body));
        } catch (JsonProcessingException e) {
            throw new RequestProcessingException(e);
        } catch (ValidationException e) {
            return e.toJSON().toMap();
        }

        return Map.of();
    }

    /**
     * Sets JSON Schema to validate it when request received.
     */
    public Request setSchema(String urlMapping, Map<String, Object> schema) {
        Request request = requestRepository.findByUrlMapping(urlMapping).orElseThrow(EntityNotFoundException::new);
        request.setSchema(schema);

        return requestRepository.save(request);
    }

    /**
     * Clear JSON Schema to validate it when request received.
     */
    public Request clearSchema(String urlMapping) {
        return setSchema(urlMapping, null);
    }

    private Request createOne(String urlMapping, RequestCreateForm form) {
        Request request = new Request();

        request.setUrlMapping(urlMapping);
        request.setCreatedAt(ZonedDateTime.now());
        request.setStatus(null == form.getStatus() ? HttpStatus.OK.value() : HttpStatus.valueOf(form.getStatus()).value());

        inflateResponses(request, form);
        return requestRepository.save(request);
    }

    private void inflateResponses(Request request, RequestCreateForm form) {
        if (null == form.getResponses() || form.getResponses().isEmpty()) {
            RequestResponse requestResponse = inflateResponse(request, null);
            request.setResponses(Lists.newArrayList(requestResponse));
            return;
        }

        List<RequestResponse> responses = form
                .getResponses()
                .stream()
                .map(r -> inflateResponse(request, r))
                .collect(Collectors.toList());

        request.setResponses(responses);
    }

    private RequestResponse inflateResponse(Request request, RequestResponseCreateForm form) {
        RequestResponse requestResponse = new RequestResponse();
        requestResponse.setRequest(request);
        requestResponse.setTrigger(null == form ? null : form.getTrigger());
        requestResponse.setResponse(null == form ? configurationProvider.getDefaultResponse() : form.getBody());
        return requestResponse;
    }

    /**
     * Exports requests library as JSON file.
     */
    public File exportAsFile() throws IOException {
        List<Request> requests = requestRepository.findAll();

        File tempFile = createTempFile(UUID.randomUUID().toString(), ".json");
        writeStringToFile(tempFile, serializeRequest(requests), defaultCharset());

        return tempFile;
    }

    /**
     * Imports requests library from JSON file.
     */
    public void importFromFile(InputStream inputStream) throws IOException {
        List<Request> requests = deserializeRequests(IOUtils.toString(inputStream, defaultCharset()));
        requests.forEach(requestRepository::save);
    }

    private String serializeRequest(List<Request> requests) {
        try {
            return objectMapper.writeValueAsString(requests);
        } catch (JsonProcessingException e) {
            return "[]";
        }
    }

    private List<Request> deserializeRequests(String raw) {
        try {
            return objectMapper.readValue(raw, new TypeReference<List<Request>>() {
            });
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }

    private void snapshotRequest(Request request, Map<String, Object> parameters, Map<String, Object> body) {
        RequestState state = new RequestState();

        state.setBody(body);
        state.setRequest(request);
        state.setSchema(request.getSchema());
        state.setUrlMapping(request.getUrlMapping());
        state.setCreatedAt(ZonedDateTime.now());
        state.setStatus(request.getStatus());
        state.setParameters(parameters);
        state.setSchemaValidationResult(checkSchema(request, body));

        requestStateRepository.save(state);
    }

    public Map<String, Object> getSchema(String urlMapping) {
        return requestRepository.findByUrlMapping(urlMapping).orElseThrow(EntityNotFoundException::new).getSchema();
    }
}

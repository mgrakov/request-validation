/**
 * Copyright (c) 2019 Lineate LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package com.lineate.request.stubbing.converter;

import com.lineate.request.stubbing.entity.request.Request;
import com.lineate.request.stubbing.entity.request.RequestResponse;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Request converter.
 */
@Service
public class RequestResponseConverter {

    /**
     * Converts request to user's response.
     */
    public Map<String, Object> from(Request request, Map<String, Object> body) {
        if (null == request) {
            return null;
        }

        return request
                .getResponses()
                .stream()
                .filter(r -> r.getTrigger() == null || body.containsKey(r.getTrigger()))
                .map(RequestResponse::getResponse)
                .reduce(this::mergeResponses)
                .orElse(new HashMap<>());
    }

    private Map<String, Object> mergeResponses(Map<String, Object> o1, Map<String, Object> o2) {
        o2.putAll(o1);
        return o2;
    }

}

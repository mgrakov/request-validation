/**
 * Copyright (c) 2019 Lineate LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package com.lineate.request.stubbing.endpoint;

import com.lineate.request.stubbing.converter.RequestResponseConverter;
import com.lineate.request.stubbing.converter.RequestStateConverter;
import com.lineate.request.stubbing.entity.request.Request;
import com.lineate.request.stubbing.respose.RequestStateResponse;
import com.lineate.request.stubbing.service.RequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.apache.commons.io.IOUtils.copy;
import static org.springframework.http.HttpStatus.valueOf;
import static org.springframework.http.ResponseEntity.ok;

/**
 * Controller for POST request processing, stubbing and settings.
 */
@RestController
@RequestMapping("api/v1/request")
public class RequestEndpoint {

    private final RequestStateConverter requestStateConverter;
    private final RequestResponseConverter requestResponseConverter;
    private final RequestService requestService;

    public RequestEndpoint(RequestStateConverter requestStateConverter,
                           RequestResponseConverter requestResponseConverter,
                           RequestService requestService) {

        this.requestStateConverter = requestStateConverter;
        this.requestResponseConverter = requestResponseConverter;
        this.requestService = requestService;
    }

    /**
     * Requests a POST with stubbed response or default parsed request body in response if unset.
     */
    @PostMapping("{urlMapping}")
    public ResponseEntity<Map<String, Object>> requestPost(@PathVariable String urlMapping,
                                                           @RequestParam Map<String, Object> params,
                                                           @RequestBody Map<String, Object> body) {
        Request request = requestService.request(urlMapping, params, body);

        return new ResponseEntity<>(
                requestResponseConverter.from(request, body),
                valueOf(request.getStatus()));
    }

    /**
     * Requests a POST with stubbed response or default parsed request body in response if unset.
     */
    @PutMapping("{urlMapping}")
    public ResponseEntity<Map<String, Object>> requestPut(@PathVariable String urlMapping,
                                                          @RequestParam Map<String, Object> params,
                                                          @RequestBody Map<String, Object> body) {
        Request request = requestService.request(urlMapping, params, body);

        return new ResponseEntity<>(
                requestResponseConverter.from(request, body),
                valueOf(request.getStatus()));
    }

    /**
     * Sets custom response to POST request by url mapping.
     */
    @PutMapping("{urlMapping}/schema")
    public Map<String, Object> setSchema(@PathVariable String urlMapping,
                                         @RequestBody Map<String, Object> schema) {
        requestService.setSchema(urlMapping, schema);
        return schema;
    }

    /**
     * Gets custom response to POST request by url mapping.
     */
    @GetMapping("{urlMapping}/schema")
    public Map<String, Object> getSchema(@PathVariable String urlMapping) {
        return requestService.getSchema(urlMapping);
    }

    /**
     * Sets custom response to POST request by url mapping.
     */
    @DeleteMapping("{urlMapping}/schema")
    public ResponseEntity clearSchema(@PathVariable String urlMapping) {
        requestService.clearSchema(urlMapping);
        return ok().build();
    }

    /**
     * Clears mock for POST request by url mapping.
     */
    @PutMapping("{urlMapping}/unset")
    public ResponseEntity unsetMapping(@PathVariable String urlMapping) {
        requestService.unset(urlMapping);
        return ok().build();
    }

    @GetMapping("{urlMapping}/history")
    public List<RequestStateResponse> findHistory(@PathVariable String urlMapping) {
        return requestService.findHistory(urlMapping).stream().map(requestStateConverter::from).collect(Collectors.toList());
    }

    @DeleteMapping("{urlMapping}/history")
    public ResponseEntity clearHistory(@PathVariable String urlMapping) {
        requestService.clearHistory(urlMapping);
        return ok().build();
    }

    /**
     * Exports requests mock library as JSON file.
     */
    @GetMapping("export")
    public void exportRequests(HttpServletResponse response) throws IOException {
        response.setContentType("application/force-download");
        response.setHeader("Content-Disposition", "attachment; filename=requests.json");
        copy(new FileInputStream(requestService.exportAsFile()), response.getOutputStream());
        response.flushBuffer();
    }

    /**
     * Imports requests mock library from JSON file.
     */
    @PostMapping("import")
    public ResponseEntity<Object> importRequests(@RequestParam("file") MultipartFile file) throws IOException {
        requestService.importFromFile(file.getInputStream());
        return ok().build();
    }

}

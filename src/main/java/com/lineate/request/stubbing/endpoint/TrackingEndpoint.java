/**
 * Copyright (c) 2019 Lineate LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package com.lineate.request.stubbing.endpoint;

import com.lineate.request.stubbing.converter.TrackingConverter;
import com.lineate.request.stubbing.respose.TrackingHistoryResponse;
import com.lineate.request.stubbing.service.TrackingService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

/**
 * Controller for GET request tracking.
 */
@RestController
@RequestMapping("api/v1/tracking")
public class TrackingEndpoint {

    private final TrackingConverter trackingConverter;
    private final TrackingService trackingService;

    public TrackingEndpoint(TrackingConverter trackingConverter, TrackingService trackingService) {
        this.trackingConverter = trackingConverter;
        this.trackingService = trackingService;
    }

    /**
     * Tracks request's parameters and returns 200 OK.
     */
    @GetMapping
    public ResponseEntity trackGetRequest(@RequestParam Map<String, Object> params) {
        trackingService.track(params);
        return ResponseEntity.ok().build();
    }

    /**
     * Tracks request's parameters and returns 200 OK.
     */
    @DeleteMapping
    public ResponseEntity trackDeleteRequest(@RequestParam Map<String, Object> params) {
        trackingService.track(params);
        return ResponseEntity.ok().build();
    }

    /**
     * Shows tracked history.
     */
    @GetMapping(value = "history")
    public List<TrackingHistoryResponse> findHistory() {
        return trackingService.findHistory().stream().map(trackingConverter::from).collect(toList());
    }

    /**
     * Clears tracked history.
     */
    @DeleteMapping(value = "history")
    public ResponseEntity clearHistory() {
        trackingService.clearHistory();
        return ResponseEntity.ok().build();
    }
}

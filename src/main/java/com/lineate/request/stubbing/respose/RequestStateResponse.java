/**
 * Copyright (c) 2019 Lineate LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */
package com.lineate.request.stubbing.respose;

import java.time.ZonedDateTime;
import java.util.Map;

/**
 * UI response of request's snapshot.
 */
public class RequestStateResponse {

    private ZonedDateTime timestamp;

    private Map<String, Object> body;

    private Map<String, Object> parameters;

    private Integer status;

    private Map<String, Object> response;

    private Map<String, Object> schemaValidationResult;

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }

    public RequestStateResponse setTimestamp(ZonedDateTime timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public Map<String, Object> getBody() {
        return body;
    }

    public RequestStateResponse setBody(Map<String, Object> body) {
        this.body = body;
        return this;
    }

    public Map<String, Object> getResponse() {
        return response;
    }

    public RequestStateResponse setResponse(Map<String, Object> response) {
        this.response = response;
        return this;
    }

    public Map<String, Object> getSchemaValidationResult() {
        return schemaValidationResult;
    }

    public RequestStateResponse setSchemaValidationResult(Map<String, Object> schemaValidationResult) {
        this.schemaValidationResult = schemaValidationResult;
        return this;
    }

    public Integer getStatus() {
        return status;
    }

    public RequestStateResponse setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public Map<String, Object> getParameters() {
        return parameters;
    }

    public RequestStateResponse setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
        return this;
    }
}

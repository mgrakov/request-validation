# Request stubbing tool

**The simple solution to create simple mock API to test third party services integration without real remote requests**

### Features

- Tracking of any of GET requests parameters
- Stubbing POST requests with default body
- Stubbing POST requests with custom response
- Stubbing POST requests with custom status code and exception message
- JSON Schema validation tool
- Export / import mocks collection to / from JSON file

#### Tracking GET Requests

To track parameters send GET request to http://localhost:5050/api/v1/tracking with any request parameters. Request processing finished with 200 OK and you may see tracked and parsed arguments by http://localhost:5050/api/v1/tracking/history

#### POST request stubbing

To stub request with default body, make POST request to http://localhost:5050/api/v1/request/{url-mapping-name} with following body format

```
{
	"status": 200,
	"responses": [
		{
			"trigger": "one",
			"body": {"date": "today"}
		},
		{
			"trigger": "two",
			"body": {"money": 20000}
		},
		{
			"body": {"alwaysHere": "me too!"}
		}
	]
}
```
Where
* Status - it's response status code
* Responses - it's chunks of responses which may be returned from request if body contains *trigger* key. When *trigger* is null, body of these response will be returned always. When responses isn't presented, default response will be set from default configuration

Request processing finished with requested status and you may see stubbed response with empty body. After initialization, you may send any bodies to the same url-mapping-name and see processed response

#### POST requests history

To see history of requests, you may GET request to http://localhost:5050/api/v1/request/{url-mapping-name}/history

To clear history of requests, you may DELETE request to same url
 
 #### POST request JSON schema validation
 
 In order to set JSON schema validation to initialized firstly POST request, you need send PUT request to http://localhost:5050/api/v1/request/{url-mapping-name}/schema with JSON schema as body. You may sent any POST requests to http://localhost:5050/api/v1/request/{url-mapping-name} and each times request's data will be validated by this schema.
 
 Also, you may clear schema handling via DELETE request to http://localhost:5050/api/v1/request/{url-mapping-name}/schema
 
#### Export mocks as JSON file

To export request stubbing settings as JSON file, send GET request to http://localhost:5050/api/v1/request/export and save requested file to file system

#### Import mocks from JSON file

To import request stubbing settings from JSON file exported before, make POST request to http://localhost:5050/api/v1/request/import with "file" parameter of data-form. All stubs will be added to existsing, but not overwritten

### Application running with Docker

In order to run Application with Docker you just need navigate to Application folder and type:

``` 
docker-compose build
docker-compose up
```

### Application building

In order to build application you need:

* JDK 8+
* Gradle 
* Postgres to run application in persistent mode

When required set is installed, type following in command line

``` 
gradle bootJar
```

Then you may run application via

``` 
 java -jar build/libs/request-stubbing-1.0-SNAPSHOT.jar --spring.profiles.active=transient
```

In this mode Application runned in-memory mode. All data will be recorded only while application is alive

To run Application in persistent mode, you need install Postgres and create DB with following settings:
``` 
 url: jdbc:postgresql://localhost:5432/request_stubbing
 user: request_stubbing
 password = fwHKhf9P
``` 

Then, it should be migrated with flyway plugin via

``` 
 gradle flywayMigrate
```

Then you may run Application via following way

``` 
 java -jar build/libs/request-stubbing-1.0-SNAPSHOT.jar --spring.profiles.active=persistent
```

### API documentation

Api documentation is available in [Swagger](http://localhost:5050/swagger-ui.html#/)